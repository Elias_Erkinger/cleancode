package at.htl.gui;

import at.htl.service.AppService;

import javax.swing.*;
import java.awt.*;

public class AppFrame extends JFrame {

    private AppService appService;

    public AppFrame(AppService appService) {
        this.appService = appService;
        this.setTitle("DBAccess");
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);

        Dimension size = this.calcSize();
        this.setSize(size);

        this.setLocationRelativeTo(null);
        this.setVisible(true);

        this.setLayout(new BorderLayout());
        this.addComponents();
    }

    private void addComponents() {
        JTabbedPane tabbedPane = new JTabbedPane();

        ListPanel customerPanel = new CustomerPanel(appService);
        ListPanel productPanel = new ProductPanel(appService);

        tabbedPane.add("Customer",customerPanel);
        tabbedPane.add("Product",productPanel);

        this.add(tabbedPane,BorderLayout.CENTER);
    }


    private Dimension calcSize() {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

        double percentageToScreenWidth = 20, percentageToScreenHeight = 30;

        int screenWidth = (int) (screenSize.width * percentageToScreenWidth/100);
        int screenHeight = (int) (screenSize.height * percentageToScreenHeight/100);

        return new Dimension(screenWidth,screenHeight);
    }
}
