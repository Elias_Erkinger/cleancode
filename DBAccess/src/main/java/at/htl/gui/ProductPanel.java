package at.htl.gui;

import at.htl.entity.Product;
import at.htl.service.ProductService;
import at.htl.service.ServiceException;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ProductPanel extends ListPanel<Product> {

    private ProductService appService;

    private JTextField inputName;
    private JTextField inputPrice;

    private DefaultListModel<Product> listModel;

    public ProductPanel(ProductService appService){
        super(Product.class.getDeclaredFields().length);
        this.appService = appService;

        this.listModel = getListModel();
        listModel.addAll(appService.getProductList());


        this.addFieldsToEditPanel();

        this.settingDeleteAction();
        this.settingInsertAction();
        this.settingUpdateAction();

        this.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                Product currentProduct = getSelectedValue();
                if(currentProduct != null) {
                    inputName.setText(currentProduct.getName());
                    inputPrice.setText("" + currentProduct.getPrice());
                }
            }
        });
    }

    private void addFieldsToEditPanel() {
        JPanel editPanel = this.getEditPanel();

        this.inputName = this.createTextFieldAndAddTo(editPanel,"Name: ");
        this.inputPrice = this.createTextFieldAndAddTo(editPanel,"Price: ");

    }
    private JTextField createTextFieldAndAddTo(JComponent addToThis,String label){
        JTextField ret = new JTextField();
        JLabel jLabel = new JLabel(label);
        jLabel.setLabelFor(ret);

        addToThis.add(jLabel);
        addToThis.add(ret);

        return ret;
    }

    private void settingDeleteAction() {
        this.addDeleteButtonActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int indexOfValueObject = getSelectedIndex();
                try {
                    listModel.remove(indexOfValueObject);
                    appService.deleteProduct(indexOfValueObject);
                } catch (ServiceException serviceException) {
                    serviceException.printStackTrace();
                }
            }
        });
    }
    private void settingInsertAction(){
        this.addInsertButtonActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String name = inputName.getText();
                double price = Double.parseDouble(inputPrice.getText());

                Product newProduct = new Product(name,price);
                try {
                    listModel.addElement(newProduct);
                    appService.insertProduct(newProduct);
                } catch (ServiceException serviceException) {
                    serviceException.printStackTrace();
                }
            }
        });
    }
    private void settingUpdateAction(){
        this.addUpdateButtonActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Product productToUpdate = getSelectedValue();

                String name = inputName.getText();
                if("".equals(name) == false){
                    productToUpdate.setName(name);
                }
                if(inputPrice.getText().length() > 0){
                    double price = Double.parseDouble(inputPrice.getText());
                    productToUpdate.setPrice(price);
                }

                try {
                    updateList();
                    appService.updateProduct(productToUpdate);
                } catch (ServiceException serviceException) {
                    serviceException.printStackTrace();
                }
            }
        });
    }
}
