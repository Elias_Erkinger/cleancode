package at.htl.gui;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.List;

public abstract class ListPanel<T> extends JPanel{

    private DefaultListModel<T> listModel;
    private JList<T> jList;

    private JPanel editPanel;
    public JPanel getEditPanel() {
        return editPanel;
    }

    private JButton insertButton = new JButton("Insert");
    private JButton updateButton = new JButton("Update");
    private JButton deleteButton = new JButton("Delete");

    public ListPanel(int numberOfProperties) {
        super();
        this.setLayout(new BorderLayout());

        this.listModel = new DefaultListModel<>();
        this.jList = new JList<>(listModel);
        jList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        JScrollPane listScroller = new JScrollPane(jList);
        this.add(listScroller, BorderLayout.CENTER);

        JPanel eastPanel = new JPanel();
        eastPanel.setLayout(new BorderLayout());

        this.editPanel = new JPanel();
        this.editPanel.setLayout(new GridLayout(numberOfProperties,0));

        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new FlowLayout());
        buttonPanel.add(insertButton);
        buttonPanel.add(updateButton);
        buttonPanel.add(deleteButton);

        eastPanel.add(this.editPanel,BorderLayout.NORTH);
        eastPanel.add(buttonPanel,BorderLayout.SOUTH);

        JSplitPane splitContent = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,jList,eastPanel);
        this.add(splitContent);

    }

    public DefaultListModel<T> getListModel() {
        return listModel;
    }

    public void addDeleteButtonActionListener(ActionListener deleteButtonAction){
        this.deleteButton.addActionListener(deleteButtonAction);
    }

    public void addInsertButtonActionListener(ActionListener insertButtonAction){
        this.insertButton.addActionListener(insertButtonAction);
    }
    public void addUpdateButtonActionListener(ActionListener updateButtonAction){
        this.updateButton.addActionListener(updateButtonAction);
    }

    public int getSelectedIndex(){
        return this.jList.getSelectedIndex();
    }
    public T getSelectedValue(){
        return this.jList.getSelectedValue();
    }

    public void addListSelectionListener(ListSelectionListener listSelectionListener){
        jList.addListSelectionListener(listSelectionListener);
    }
    public void updateList(){
        jList.updateUI();
    }
}
