package at.htl.gui;

import at.htl.entity.Customer;
import at.htl.helps.Help;
import at.htl.service.CustomerService;
import at.htl.service.ServiceException;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.util.Date;

public class CustomerPanel extends ListPanel<Customer> {

    private final CustomerService appService;

    private JTextField inputName;
    private JTextField inputBirthdate;
    private JTextField inputSal;

    private DefaultListModel<Customer> listModel;

    public CustomerPanel(CustomerService appService){
        super(Customer.class.getDeclaredFields().length);
        this.appService = appService;

        this.listModel = this.getListModel();
        this.listModel.addAll(appService.getCustomerList());
        
        this.addFieldsToEditPanel();

        this.settingDeleteAction();
        this.settingInsertAction();
        this.settingUpdateAction();

        this.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                Customer currentCustomer = getSelectedValue();
                if(currentCustomer != null) {
                    inputName.setText(currentCustomer.getName());
                    if (currentCustomer.getBirthdate() != null) {
                        inputBirthdate.setText("" + Help.getFormattedDate(currentCustomer.getBirthdate()));
                    } else {
                        inputBirthdate.setText("");
                    }
                    inputSal.setText("" + currentCustomer.getSal());
                }
            }
        });
    }

    private void addFieldsToEditPanel() {
        JPanel editPanel = this.getEditPanel();

        this.inputName = this.createTextFieldAndAddTo(editPanel,"Name: ");
        this.inputBirthdate = this.createTextFieldAndAddTo(editPanel,"Birthdate: ");
        this.inputSal = this.createTextFieldAndAddTo(editPanel,"Salary: ");

    }
    private JTextField createTextFieldAndAddTo(JComponent addToThis,String label){
        JTextField ret = new JTextField();
        JLabel jLabel = new JLabel(label);
        jLabel.setLabelFor(ret);

        addToThis.add(jLabel);
        addToThis.add(ret);

        return ret;
    }

    private void settingDeleteAction(){
        this.addDeleteButtonActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int indexOfValueObject = getSelectedIndex();
                try {
                    listModel.remove(indexOfValueObject);
                    appService.deleteCustomer(indexOfValueObject);
                } catch (ServiceException serviceException) {
                    serviceException.printStackTrace();
                }
            }
        });
    }
    private void settingInsertAction(){
        this.addInsertButtonActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Customer newCustomer = readFromInput();
                try {
                    listModel.addElement(newCustomer);
                    appService.insertCustomer(newCustomer);
                } catch (ServiceException serviceException) {
                    serviceException.printStackTrace();
                }
            }
        });
    }
    private void settingUpdateAction(){
        this.addUpdateButtonActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Customer customerToUpdate = getSelectedValue();

                if(!inputName.getText().isEmpty()) customerToUpdate.setName(inputName.getText());
                if(!inputBirthdate.getText().isEmpty()) customerToUpdate.setBirthdate(readBirthdateFromInput());
                if(!inputSal.getText().isEmpty()) customerToUpdate.setSal(Double.parseDouble(inputSal.getText()));

                try {
                    updateList();
                    appService.updateCustomer(customerToUpdate);
                } catch (ServiceException serviceException) {
                    serviceException.printStackTrace();
                }
            }
        });
    }

    private Customer readFromInput(){
        String name;
        if(inputName.getText().length() > 0){
            name = inputName.getText();
        }else{
            throw new IncompleteUserForm();
        }

        Date birthdate = readBirthdateFromInput();

        double sal;
        try {
            sal = Double.parseDouble(inputSal.getText());
        } catch (Exception exception) {
            throw new IncompleteUserForm();
        }

        Customer newCustomer = new Customer(name,birthdate,sal);
        return newCustomer;
    }

    private Date readBirthdateFromInput(){
        try{
            return Help.getDateFromFormatString(inputBirthdate.getText());
        }catch (ParseException parseException){
            throw new IncompleteUserForm();
        }
    }
}
