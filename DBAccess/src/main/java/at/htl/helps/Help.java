package at.htl.helps;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public final class Help {

    private static DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

    public static void setDateFormat(DateFormat format){
        Help.format = format;
    }
    public static String getFormattedDate(Date date){
        return Help.format.format(date);
    }
    public static Date getDateFromFormatString(String date) throws ParseException {
        return Help.format.parse(date);
    }
    public static DateFormat getDateFormat(){
        return Help.format;
    }
}
