package at.htl.entity;

public interface Identifiable {
    public int getId();
    public void setId(int id);
}
