package at.htl.entity;

import java.util.Date;

public class Customer implements Identifiable{
    private String name;
    private Date birthdate;
    private double sal;
    private int id = -1;

    public Customer(String name, Date birthdate, double sal) {
        this.setName(name);
        this.setBirthdate(birthdate);
        this.setSal(sal);
    }

    @Override
    public String toString() {
        return getName();
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public double getSal() {
        return sal;
    }

    public void setSal(double sal) {
        this.sal = sal;
    }
}
