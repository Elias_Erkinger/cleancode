package at.htl.service;

import at.htl.entity.Product;

import java.util.List;

public interface ProductService {
    List<Product> getProductList();
    void deleteProduct(int index) throws ServiceException;
    void updateProduct(Product product) throws ServiceException;
    void insertProduct(Product product) throws ServiceException;
}
