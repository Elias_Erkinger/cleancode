package at.htl.service;

import at.htl.dao.CustomerDao;
import at.htl.dao.DaoException;
import at.htl.dao.ProductDao;
import at.htl.entity.Customer;
import at.htl.entity.Product;

import java.util.List;

public class AppServiceImp implements AppService{

    private CustomerDao customerDAO;
    private ProductDao productDAO;

    private List<Product> productList;
    private List<Customer> customerList;

    public AppServiceImp(CustomerDao customerDAO, ProductDao productDAO) throws ServiceException{
        this.customerDAO = customerDAO;
        this.productDAO = productDAO;

        this.readListsFromDAO();
    }

    private void readListsFromDAO() throws ServiceException{
        try{
            this.customerList = this.customerDAO.list();
            this.productList = this.productDAO.list();
        }catch (DaoException daoException){
            daoException.printStackTrace();
            throw new ServiceException();
        }
    }

    @Override
    public List<Product> getProductList() {
        return this.productList;
    }
    @Override
    public List<Customer> getCustomerList() {
        return this.customerList;
    }

    @Override
    public void deleteProduct(int index) throws ServiceException {
        try {
            Product productToDelete = this.getProductList().get(index);
            this.productDAO.delete(productToDelete);
        }catch (DaoException | RuntimeException ex){
            throw new ServiceException();
        }
    }
    @Override
    public void deleteCustomer(int index) throws ServiceException {
        try {
            Customer customerToDelete = this.getCustomerList().get(index);
            this.customerDAO.delete(customerToDelete);
        }catch (DaoException | RuntimeException daoException){
            throw new ServiceException();
        }
    }

    @Override
    public void updateProduct(Product product) throws ServiceException {
        try{
            this.productDAO.update(product);
        } catch (DaoException daoException){
            throw new ServiceException();
        }
    }
    @Override
    public void updateCustomer(Customer customer) throws ServiceException {
        try{
            this.customerDAO.update(customer);
        } catch (DaoException daoException){
            throw new ServiceException();
        }
    }

    @Override
    public void insertProduct(Product product) throws ServiceException {
        try{
            productDAO.create(product);
        }catch (DaoException daoException){
            throw new ServiceException();
        }
    }
    @Override
    public void insertCustomer(Customer customer) throws ServiceException {
        try{
            customerDAO.create(customer);
        }catch (DaoException daoException){
            throw new ServiceException();
        }
    }
}
