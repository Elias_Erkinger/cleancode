package at.htl.service;

import at.htl.entity.Customer;
import java.util.List;

public interface CustomerService {
    List<Customer> getCustomerList();
    void deleteCustomer(int index) throws ServiceException;
    void updateCustomer(Customer customer) throws ServiceException;
    void insertCustomer(Customer customer) throws ServiceException;
}
