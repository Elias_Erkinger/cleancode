package at.htl;

import at.htl.dao.DaoProperties;
import at.htl.gui.AppFrame;
import at.htl.dao.CustomerJdbcDao;
import at.htl.dao.ProductJdbc_Dao;
import at.htl.service.AppServiceImp;
import at.htl.service.ServiceException;

import javax.swing.*;
import java.lang.reflect.InvocationTargetException;

public class Main {
    public static void main(String[] args) throws InvocationTargetException, InterruptedException {
        SwingUtilities.invokeAndWait(()->{
            try {
                System.setProperty(DaoProperties.PROPERTY_FILE_PATH_SYSTEM_PROPERTY, "src/at/htl/dao/db.properties");
                JFrame frame = new AppFrame(new AppServiceImp(new CustomerJdbcDao(),new ProductJdbc_Dao()));
            } catch (ServiceException serviceException) {
                serviceException.printStackTrace();
            }
        });
    }
}
