package at.htl.dao;

import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Properties;

public class DaoProperties {
    private final String PROPERTIES_PATH;

    private static volatile DaoProperties singleton;

    public static synchronized DaoProperties getInstance() throws IOException {
        if(singleton == null){
            singleton = new DaoProperties();
        }
        return singleton;
    }

    private DaoProperties() throws IOException {
        PROPERTIES_PATH = System.getProperty(PROPERTY_FILE_PATH_SYSTEM_PROPERTY);
        propertiesReader = new Properties();
        propertiesReader.load(new FileReader(PROPERTIES_PATH));
    }

    public boolean isLocal(){
        return propertiesReader.getProperty("local").equals("true");
    }

    private Properties propertiesReader;

    public static final String PROPERTY_FILE_PATH_SYSTEM_PROPERTY = "dbPropertyFile";
    public static final String DRIVER_URL_PROPERTY_NAME = "jdbcURL";
    public static final String DATABASE_URL_PROPERTY_NAME = "dbURL";
    public static final String URL_SETTING_PROPERTY_NAME = "urlSettings";
    public static final String DATABASE_USER_PROPERTY_NAME = "dbUser";
    public static final String DATABASE_PASSWORD_PROPERTY_NAME = "dbPW";

    public String getStringProperty(String propertyName) {
        return propertiesReader.getProperty(propertyName);
    }

}
