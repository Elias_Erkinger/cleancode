package at.htl.dao;

import at.htl.entity.Customer;

public interface CustomerDao extends Crud<Customer> {

}
