package at.htl.dao;

import java.util.List;

public interface Crud<T> {

    public void create(T t) throws DaoException;
    public T read(int id) throws DaoException;
    public void update(T t) throws DaoException;
    public void delete(T t) throws DaoException;
    public List<T> list() throws DaoException;

}
