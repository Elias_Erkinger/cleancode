package at.htl.dao;

public class DaoException extends Exception{
    private String daoMessage = "";

    public DaoException(String message) {
        daoMessage = message;
    }
    public DaoException(){}

    public String getDaoMessage(){
        return daoMessage;
    }
}
