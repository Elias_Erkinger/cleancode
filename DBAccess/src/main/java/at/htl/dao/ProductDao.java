package at.htl.dao;

import at.htl.entity.Product;
import java.util.List;

public interface ProductDao extends Crud<Product>{

}
