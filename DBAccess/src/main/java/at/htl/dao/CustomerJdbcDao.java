package at.htl.dao;

import at.htl.entity.Customer;

import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class CustomerJdbcDao extends BaseJdbcDao<Customer> implements CustomerDao {

    public CustomerJdbcDao(){
        super("customer","ID");
    }

    @Override
    protected Customer getPojoFromResultSet(ResultSet result) throws SQLException {
        final String name = result.getString("Name");
        final Date birthdate = result.getDate("Birthdate");
        final double sal = result.getDouble("Sal");

        Customer customer = new Customer(name,birthdate,sal);
        customer.setId(result.getInt(getPkName()));
        return customer;
    }

    @Override
    protected PreparedStatement getUpdateStatement(Connection connection, Customer customer) throws SQLException {
        String sql = "UPDATE " + this.getTablename() + " SET " + "NAME = '" + customer.getName() + "', ";
        sql += "BIRTHDATE = " + getSQLDateString(customer.getBirthdate()) + ", SAL = '" + customer.getSal() + "' ";
        sql += "WHERE " + getPkName() + " = " + customer.getId() + ";";

        return connection.prepareStatement(sql);
    }

    @Override
    protected PreparedStatement getInsertStatement(Connection connection, Customer customer) throws SQLException {
        String sql = "INSERT INTO " + this.getTablename() + " (NAME, BIRTHDATE, SAL)";
        sql = sql + " VALUES('" + customer.getName() + "', " + getSQLDateString(customer.getBirthdate()) +   ", '" + customer.getSal() + "');";

        return connection.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
    }



    private String getSQLDateString(java.util.Date date){
        DateFormat englishFormat = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = englishFormat.format(date);

        return "TO_DATE('" + dateString + "', 'YYYY-MM-DD')";
    }
}
