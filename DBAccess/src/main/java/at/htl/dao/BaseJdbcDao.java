package at.htl.dao;

import at.htl.entity.Identifiable;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class BaseJdbcDao<T extends Identifiable> implements Crud<T>{
    private final String TABLENAME;
    private final String PKNAME;

    protected abstract T getPojoFromResultSet(ResultSet result) throws SQLException;
    protected abstract PreparedStatement getUpdateStatement(Connection connection, T t)throws SQLException;
    protected abstract PreparedStatement getInsertStatement(Connection connection,T t)throws SQLException;


    public BaseJdbcDao(String tablename, String pkName) {
        this.TABLENAME = tablename;
        this.PKNAME = pkName;
    }
    public String getTablename() {
        return TABLENAME;
    }
    public String getPkName() {
        return PKNAME;
    }


    private PreparedStatement getPreparedStatement(Connection c, String sql, int id) throws SQLException {
        PreparedStatement stmt = c.prepareStatement(sql);
        stmt.setInt(1, id);
        return stmt;
    }

    public final void delete(T t) throws DaoException {
        if (t.getId() < 0) return;

        String sql = "DELETE FROM " + TABLENAME + " WHERE " + PKNAME + " = ?";
        try( WrappedConnection wrappedConnection = ConnectionManager.getInstance().getWrappedConnection();
             PreparedStatement statement = getPreparedStatement(wrappedConnection.getConn(), sql, t.getId()) ){

            statement.executeUpdate();

        } catch (SQLException | IOException ex) {
            Logger.getLogger(BaseJdbcDao.class.getName()).log(Level.SEVERE, null, ex);
            throw new DaoException(ex.getMessage());
        }
    }
    //a template method
    public final T read(int id) throws DaoException {
        T t = null;
        String sql = "SELECT * FROM " + TABLENAME + " WHERE " + PKNAME + " = ? LIMIT 1";
        try( WrappedConnection wrappedConnection = ConnectionManager.getInstance().getWrappedConnection();
             PreparedStatement statement = getPreparedStatement(wrappedConnection.getConn(), sql, id);
             ResultSet result = statement.executeQuery()) {

            if (result.next()) {
                t = getPojoFromResultSet(result);
            }

        } catch (SQLException | IOException ex) {
            Logger.getLogger(BaseJdbcDao.class.getName()).log(Level.SEVERE, null, ex);
            throw new DaoException(ex.getMessage());
        }

        return t;
    }
    //a template method
    public final List<T> list() throws DaoException {
        List<T> results = new ArrayList<>();
        String sql = "SELECT * FROM " + TABLENAME;
        try(WrappedConnection wrappedConnection = ConnectionManager.getInstance().getWrappedConnection();
            Statement statement = wrappedConnection.getConn().createStatement();
            ResultSet result = statement.executeQuery(sql)) {

            while (result.next()) {
                results.add(getPojoFromResultSet(result));
            }

        } catch (SQLException | IOException ex) {
            Logger.getLogger(BaseJdbcDao.class.getName()).log(Level.SEVERE, null, ex);
            throw new DaoException(ex.getMessage());
        }
        return results;
    }
    //a template method
    public final void update(T t) throws DaoException {
        if (t.getId() < 0)  return;

        try( WrappedConnection wrappedConnection = ConnectionManager.getInstance().getWrappedConnection();
             PreparedStatement statement = getUpdateStatement(wrappedConnection.getConn(), t)  ){

            statement.executeUpdate();

        } catch (SQLException | IOException ex) {
            Logger.getLogger(BaseJdbcDao.class.getName()).log(Level.SEVERE, null, ex);
            throw new DaoException(ex.getMessage());
        }
    }
    //a template method
    public final void create(T t) throws DaoException {
        if (t.getId() >= 0) return;

        try( WrappedConnection wrappedConnection = ConnectionManager.getInstance().getWrappedConnection();
             PreparedStatement statement = getInsertStatement(wrappedConnection.getConn(),t);
             ResultSet genKeys = (statement.executeUpdate() == 1) ? statement.getGeneratedKeys() : null ) {

            if (genKeys != null && genKeys.next() ) {
                t.setId(genKeys.getInt(1));
                System.out.println(t.getId());
            }else{
                System.out.println("Not Now John");
            }

        } catch (SQLException | IOException ex) {
            Logger.getLogger(BaseJdbcDao.class.getName()).log(Level.SEVERE, null, ex);
            throw new DaoException(ex.getMessage());
        }
    }
}
