package at.htl.dao;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class ConnectionManager {
    private static volatile ConnectionManager singleton = null;

    public static synchronized ConnectionManager getInstance() throws IOException {
        DaoProperties properties = DaoProperties.getInstance();
        if(singleton == null){
            if(properties.isLocal()){
                singleton = new LocalConnectionManager();
            } else{
                singleton = new WebConnectionManager();
            }
        }
        return singleton;
    }

    public abstract WrappedConnection getWrappedConnection() throws SQLException;
    public abstract void closeFinally();


    private static class LocalConnectionManager extends ConnectionManager {
        private WrappedConnection wrappedConnection;
        private LocalConnectionManager() throws IOException {
            super();
            DaoProperties properties = DaoProperties.getInstance();

            try {
                String url = properties.getStringProperty(DaoProperties.DRIVER_URL_PROPERTY_NAME) +
                        properties.getStringProperty(DaoProperties.DATABASE_URL_PROPERTY_NAME) +
                        properties.getStringProperty(DaoProperties.URL_SETTING_PROPERTY_NAME);
                String user = properties.getStringProperty(DaoProperties.DATABASE_USER_PROPERTY_NAME);
                String password = properties.getStringProperty(DaoProperties.DATABASE_PASSWORD_PROPERTY_NAME);

                Connection conn = DriverManager.getConnection(url, user, password);

                this.wrappedConnection = new WrappedConnection(conn, false);

            } catch (SQLException ex) {
                Logger.getLogger(ConnectionManager.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        @Override
        public WrappedConnection getWrappedConnection() throws SQLException {
            return wrappedConnection;
        }

        @Override
        public void closeFinally() {
            // close the connection at the end of the program
            try {
                this.wrappedConnection.getConn().close();
            } catch (SQLException ex) {
                Logger.getLogger(ConnectionManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }


    private static class WebConnectionManager extends ConnectionManager {

        private DataSource dataSource;

        private WebConnectionManager() throws IOException {
            super();
            DaoProperties properties = DaoProperties.getInstance();

            try {
                Context ctx = new javax.naming.InitialContext();
                String dsName = properties.getStringProperty("dataSource");
                dataSource = (DataSource) ctx.lookup("java:comp/env/" + dsName);

            } catch (NamingException ex) {
                Logger.getLogger(ConnectionManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        @Override
        public WrappedConnection getWrappedConnection() throws SQLException {
            return new WrappedConnection(dataSource.getConnection(),true);
        }

        @Override
        public void closeFinally() {
            // already closed in Wrapper
        }
    }
}
