package at.htl.dao;

import at.htl.entity.Product;
import java.sql.*;

public class ProductJdbc_Dao extends BaseJdbcDao<Product> implements ProductDao {

    public ProductJdbc_Dao() {
        super("product", "ID");
    }

    @Override
    protected Product getPojoFromResultSet(ResultSet result) throws SQLException {
        final String name = result.getString("Name");
        final double price = result.getDouble("Price");

        Product product = new Product(name,price);
        product.setId(result.getInt(getPkName()));
        return product;
    }

    @Override
    protected PreparedStatement getUpdateStatement(Connection connection, Product product) throws SQLException {
        String sql = "UPDATE " + this.getTablename() + " SET " + "NAME = '" + product.getName() + "', ";
        sql += "PRICE = '" + product.getPrice() + "' WHERE " + this.getPkName() + " = " + product.getId() + ";";

        return connection.prepareStatement(sql);
    }

    @Override
    protected PreparedStatement getInsertStatement(Connection connection, Product product) throws SQLException {
        String sql = "INSERT INTO " + this.getTablename() + " (NAME, PRICE)";
        sql = sql + " Values('" + product.getName() + "', '" + product.getPrice() + "');";

        return connection.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
    }
}
